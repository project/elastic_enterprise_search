# Elastic Enterprise Search

**Old version: [https://drupal.org/project/elastic_appsearch](https://drupal.org/project/elastic_appsearch)**

This module is similar to [Search API Solr](https://www.drupal.org/project/search_api_solr "Search API Solr") What _Search API Solr_ is for Solr, this is for **_[Elastic Enterprise Search](https://www.elastic.co/enterprise-search/)_**.

## Submodules

This module currently have only one submodule called appsearch, that allow drupal to connect with elastic apps search. In future we may add one more submodule to connect with elastic workplace search.

1. Elastic App Search (Added)
1. Elastic Workplace Search (To be added in future)

This module allows Drupal to connect with [Elastic Enterprise Search](https://www.elastic.co/enterprise-search/ "Elastic Enterprise Enterprise"). Elastic Enterprise search is an elastic product that is built on top of elastic search. Elastic enterprise search provides the capability to add search functionality to any website. This also provides search analytics along with the search. The features of Elastic Enterprise Search are:

1. Top search keywords
2. Top search keywords with no results
3. Top search results with clicks
4. Top search results with no clicks
5. UI to add synonyms
6. Ability to curate search result
7. Pin certain results on top for a specific keyword
8. Ability to adjust the weight on indexed fields
9. Ability to create multiple search engine
10. Fine tune the search results
11. Ability to choose search filters, sorting options and search result title and link

## Dependency

- [Search API](https://www.drupal.org/project/search_api)
- [Elastic Enterprise Search PHP SDK](https://github.com/elastic/enterprise-search-php)
- [Pathauto](https://www.drupal.org/project/pathauto)
- [Guzzle 6 HTTP Adapter](https://github.com/php-http/guzzle6-adapter) (if you are using D9)
- [Guzzle 7 HTTP Adapter](https://github.com/php-http/guzzle7-adapter) (if you are using D10)

This module uses [https://github.com/elastic/enterprise-search-php](https://github.com/elastic/enterprise-search-php) PHP SDK to do all the operations with Elastic Enterprise search.

## Running Elastic enterprise search with docker

1. Follow official guide to run [elastic enterprise search 7.17](https://www.elastic.co/guide/en/enterprise-search/7.17/docker.html) in local.
1. Follow official guide to run [elastic enterprise search 8.7](https://www.elastic.co/guide/en/enterprise-search/8.7/docker.html) in local.
1. If you are using ddev and want to run elastic enterprise search 8.7 as a part of your ddev stack. You can use the following steps:
    1. Copy the provided `.env` and `docker-compose.enterprise-search.yaml` file in `docker-compse/ddev folder` to `.ddev` folder of your project.
    1. Run the `ddev start` or `ddev restart` command.
    1. Use the `http://enterprisesearch:3002` as the elastic enterprise search server url.

## Installation

1. Use composer for getting all the module dependencies
1. Enable module
1. Go to Configuration -> Search and metadata -> Elastic AppSearch
1. Add Engine by giving the Appsearch server credentials and enable the server
1. Add Engine by giving the engine details and content types you need to index.
1. Add Schema by selecting the fields you want to index and select the field type.
1. Click on the newly created engine and Click on index now to index all the documents
1. Go to Configuration -> Search and metadata -> Search UI and create a new search UI entity.
1. Now you will have a search block available in block configuration which can be enabled on any page.

## Features

1. Ability to create Multiple Server
2. Ability to create multiple Engines within that server
3. Ability to choose the fields for indexing for each engine
4. Ability to choose the field types for schema
5. Ability to create multiple UI for each search engine
6. Each Search UI is available as a drupal block that can be enabled on any page
7. Ability to choose content types for indexing for each engine
8. Batch indexing of documents

## Limitation

Elastic Enterprise Search provides the ability to index a maximum of 64 fields in each document. There is no option to delete the previously indexed field. You have to delete the complete engine and recreate the new one with the new schema. See https://www.elastic.co/guide/en/app-search/current/limits.html

## Overriding Search-UI component

The search-ui is a react application that is available as a drupal block. Sometimes you may want to override the search-ui react component. For example, you may want to add a new field in result component based on content type or change the look and feel of the search-ui. By default search ui result component display the search result in key value pair.
![search ui default result view](../elastic_enterprise_search/docs/search-ui-default.png)

But search-ui allows you to override the result component by providing a `resultView` props to Result component `<Results resultView={CustomResultView} />`. We have created a custom result component that you can find in `modules/appsearch/search-ui/src/components/ResultView/index.js`. You can override this component by creating a new component in your custom module by following the below steps:

1. Create a custom module called something like `my_custom_searchui` under `docroot/modules/custom` folder.
1. Create the component file `docroot/modules/custom/my_custom_searchui/ResultView/index.js` and copy the content from `modules/appsearch/search-ui/src/components/ResultView/index.js` file.
1. Make the changes you want to make in the component.
1. Add the following line in your composer.json file.

    ```json
    "scripts": {
      "post-install-cmd": [
          "cp -rf docroot/modules/custom/my_custom_searchui/* docroot/modules/custom/elastic_enterprise_search/modules/appsearch/search-ui/src/searchui_components_override",
          "cd docroot/modules/custom/elastic_enterprise_search/modules/appsearch/search-ui && npm install && npm run build"
      ]
    }
    ```

1. Run the composer install command `composer install` this will copy your component to the search-ui folder and build the search-ui.

**Note:** This is a current limitation because the search-ui is a react application and we have to build the application to see the changes and react application can not include the component from outside the application. See `getComponent` function in `modules/appsearch/search-ui/src/utils/index.tsx` file.

### Resources

1. [Customizing Result Component](https://docs.elastic.co/search-ui/api/react/components/result#view-customization)
1. [Codesandbox Demo of Search UI](https://codesandbox.io/s/github/elastic/search-ui/tree/main/examples/sandbox?from-embed=&initialpath=/app-search&file=/src/pages/app-search/index.js)
1. [Elastic Search UI](https://github.com/elastic/search-ui)

## Roadmap

1. Add test connection feature on server page.
1. Remove dependency on pathauto and search api module.
1. Add support for other entities apart from nodes.
1. ~~Add support for elastic Date field that is being being indexed as number as of now.~~ - **Done**
1. Add support for both asec and desc sort filters.
