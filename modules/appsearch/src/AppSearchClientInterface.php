<?php

namespace Drupal\appsearch;

/**
 * Interface AppSearchClientInterface for AppSearch client.
 */
interface AppSearchClientInterface {

  /**
   * Get the AppSearch client instance.
   *
   * @param mixed $server
   *   The server entity.
   *
   * @return mixed
   *   The AppSearch client instance.
   */
  public static function getInstance($server);

  /**
   * Connect to AppSearch.
   */
  public static function connect($server, $privateKey, $engine = FALSE);

}
