<?php

namespace Drupal\appsearch;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\appsearch\Entity\EngineInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Provides a listing of Engine entities.
 */
class EngineListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Engine');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = [
      'data' => [
        '#type' => 'link',
        '#title' => $entity->label(),
      ] + $entity->toUrl('canonical')->toRenderArray(),
      'class' => ['search-api-title'],
    ];
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity instanceof EngineInterface) {
      $route_parameters['appsearch_engine'] = $entity->id();
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 20,
        'url' => new Url('appsearch.appsearch_engine.canonical', $route_parameters),
      ];
    }

    return $operations;
  }

}
