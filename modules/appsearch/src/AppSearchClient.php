<?php

namespace Drupal\appsearch;

use Elastic\EnterpriseSearch\Client;
use Elastic\EnterpriseSearch\AppSearch\Request\GetEngine;
use Elastic\EnterpriseSearch\AppSearch\Request\ListEngines;

/**
 * AppSearchClient service to connect to AppSearch.
 */
class AppSearchClient implements AppSearchClientInterface {

  /**
   * The AppSearch client instance.
   *
   * @var array
   */
  protected static $clients;

  /**
   * {@inheritdoc}
   */
  public static function getInstance($server) {
    if (!empty(self::$clients[$server->id()])) {
      return self::$clients[$server->id()];
    }

    try {
      $client = new Client([
        'host' => $server->getHost(),
        'app-search' => [
          'token' => $server->getPrivateKey(),
        ],
      ]);
      $appSearch = $client->appSearch();
      self::$clients[$server->id()] = $appSearch;
      return self::$clients[$server->id()];
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      \Drupal::logger('appsearch')->error($e->getMessage());
      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function connect($server, $privateKey, $engine = FALSE) {
    try {
      $client = new Client([
        'host' => $server,
        'app-search' => [
          'token' => $privateKey,
        ],
      ]);
      // @todo Validate if this is enough to check if the connection is valid.
      $appSearch = $client->appSearch();
      if ($engine) {
        $engine = $appSearch->getEngine(new GetEngine($engine));
        return $engine;
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      \Drupal::logger('appsearch')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Get the list of engines of a server.
   */
  public static function getEngines($server) {
    $appSearch = self::getInstance($server);
    $engines = $appSearch->listEngines(new ListEngines());
    return $engines;
  }

}
