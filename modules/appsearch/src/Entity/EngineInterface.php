<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Engine entities.
 */
interface EngineInterface extends ConfigEntityInterface {

  /**
   * Get the language of the engine.
   */
  public function getLanguage();

  /**
   * Get the fields of datasources like fields of different type of entities.
   */
  public function getDataSourcesFields();

  /**
   * Get the fields of engine that are part of the schema.
   */
  public function getEngineFields();

}
