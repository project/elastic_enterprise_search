<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Server entity.
 *
 * @ConfigEntityType(
 *   id = "appsearch_server",
 *   label = @Translation("Appsearch Server"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\appsearch\ServerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\appsearch\Form\ServerForm",
 *       "edit" = "Drupal\appsearch\Form\ServerForm",
 *       "delete" = "Drupal\appsearch\Form\ServerDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\appsearch\ServerHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "appsearch_server",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "description" = "description",
 *     "host" = "host",
 *     "privateKey" = "privateKey",
 *     "searchKey" = "searchKey",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *     "host",
 *     "privateKey",
 *     "searchKey",
 *     "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/enterprise-search/appsearch/server/{appsearch_server}",
 *     "add-form" = "/admin/config/search/enterprise-search/appsearch/server/add",
 *     "edit-form" = "/admin/config/search/enterprise-search/appsearch/server/{appsearch_server}/edit",
 *     "delete-form" = "/admin/config/search/enterprise-search/appsearch/server/{appsearch_server}/delete",
 *     "collection" = "/admin/config/search/enterprise-search/appsearch/server"
 *   }
 * )
 */
class Server extends ConfigEntityBase implements ServerInterface {

  /**
   * The Server ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Server label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Server description.
   *
   * @var string
   */
  protected $description;

  /**
   * The Server host.
   *
   * @var uri
   */
  protected $host;

  /**
   * The Server private key.
   *
   * @var string
   */
  protected $privateKey;

  /**
   * The Server search key.
   *
   * @var string
   */
  protected $searchKey;

  /**
   * The Server status.
   *
   * @var bool
   */
  protected $status;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getHost() {
    return $this->host;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateKey() {
    return $this->privateKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchKey() {
    return $this->searchKey;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // The rest of the code only applies to updates.
    if (!isset($this->original)) {
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    return \Drupal::service('appsearch.client')::getInstance($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getEngines(array $properties = []) {
    $storage = \Drupal::entityTypeManager()->getStorage('appsearch_engine');
    return $storage->loadByProperties(['server' => $this->id()] + $properties);
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable() {
    $is_available = TRUE;
    try {
      \Drupal::service('appsearch.client')::connect(
        $this->getHost(),
        $this->getPrivateKey()
      );
    }
    catch (\Exception $e) {
      $is_available = FALSE;
      \Drupal::logger('appsearch')->notice('Unable to reach server - ' . $this->id());
    }
    return $is_available;
  }

}
