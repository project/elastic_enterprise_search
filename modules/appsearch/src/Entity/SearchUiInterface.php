<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Search Ui entities.
 */
interface SearchUiInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
