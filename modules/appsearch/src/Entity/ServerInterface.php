<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Server entities.
 */
interface ServerInterface extends ConfigEntityInterface {

  /**
   * Return the description of the server.
   *
   * @return string
   *   The description of the server.
   */
  public function getDescription();

  /**
   * Return the hostname of the server.
   *
   * @return string
   *   The hostname of the server.
   */
  public function getHost();

  /**
   * Return the status of the server.
   *
   * @return int
   *   The status of the server.
   */
  public function getStatus();

  /**
   * Return the private key of the server.
   *
   * @return int
   *   The private key of the server.
   */
  public function getPrivateKey();

  /**
   * Return the search key of the server.
   *
   * @return int
   *   The search key of the server.
   */
  public function getSearchKey();

}
