<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Search Ui entity.
 *
 * @ConfigEntityType(
 *   id = "appsearch_searchui",
 *   label = @Translation("Search Ui"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\appsearch\SearchUiListBuilder",
 *     "form" = {
 *       "add" = "Drupal\appsearch\Form\SearchUiForm",
 *       "edit" = "Drupal\appsearch\Form\SearchUiForm",
 *       "delete" = "Drupal\appsearch\Form\SearchUiDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\appsearch\SearchUiHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "appsearch_searchui",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "engine" = "engine",
 *     "field_title" = "field_title",
 *     "field_url" = "field_url",
 *     "fields_filter" = "fields_filter",
 *     "fields_filter_searchable" = "fields_filter_searchable",
 *     "fields_filter_disjunctive" = "fields_filter_disjunctive",
 *     "fields_sort" = "fields_sort",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "engine",
 *     "field_title",
 *     "field_url",
 *     "fields_filter",
 *     "fields_filter_searchable",
 *     "fields_filter_disjunctive",
 *     "fields_sort",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/enterprise-search/appsearch/searchui/{appsearch_searchui}",
 *     "add-form" = "/admin/config/search/enterprise-search/appsearch/searchui/add",
 *     "edit-form" = "/admin/config/search/enterprise-search/appsearch/searchui/{appsearch_searchui}/edit",
 *     "delete-form" = "/admin/config/search/enterprise-search/appsearch/searchui/{appsearch_searchui}/delete",
 *     "collection" = "/admin/config/search/enterprise-search/appsearch/searchui"
 *   }
 * )
 */
class SearchUi extends ConfigEntityBase implements SearchUiInterface {

  /**
   * The Search Ui ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Search Ui label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Search Ui engine.
   *
   * @var string
   */
  protected $engine;

  /**
   * The Search Ui field title.
   *
   * @var string
   */
  protected $field_title;

  /**
   * The Search Ui field url.
   *
   * @var string
   */
  protected $field_url;

  /**
   * The Search Ui field filters.
   *
   * @var array
   */
  protected $fields_filter;

  /**
   * The Search searchable field filters.
   *
   * @var array
   */
  protected $fields_filter_searchable = [];

  /**
   * The Search Ui field filters.
   *
   * @var array
   */
  protected $fields_filter_disjunctive = [];

  /**
   * The Search Ui fields sort.
   *
   * @var array
   */
  protected $fields_sort = [];

  /**
   * {@inheritdoc}
   */
  public function getEngine() {
    return $this->engine;
  }

  /**
   * {@inheritdoc}
   */
  public function getEngineInstance() {
    return \Drupal::entityTypeManager()->getStorage('appsearch_engine')->load($this->getEngine());
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldTitle() {
    return $this->field_title;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldUrl() {
    return $this->field_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsFilter() {
    return $this->fields_filter;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsSort() {
    return $this->fields_sort;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsFilterSearchable() {
    return $this->fields_filter_searchable;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsFilterDisjunctive() {
    return $this->fields_filter_disjunctive;
  }

}
