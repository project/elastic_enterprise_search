<?php

namespace Drupal\appsearch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\appsearch\Utility\Database;
use Drupal\appsearch\Utility\BatchHelper;
use Elastic\EnterpriseSearch\AppSearch\Request\GetEngine;
use Elastic\EnterpriseSearch\AppSearch\Request\IndexDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\ListDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\DeleteDocuments;
use Elastic\EnterpriseSearch\AppSearch\Schema\Document;

/**
 * Defines the Engine entity.
 *
 * @ConfigEntityType(
 *   id = "appsearch_engine",
 *   label = @Translation("Engine"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\appsearch\EngineListBuilder",
 *     "form" = {
 *       "add" = "Drupal\appsearch\Form\EngineForm",
 *       "edit" = "Drupal\appsearch\Form\EngineForm",
 *       "delete" = "Drupal\appsearch\Form\EngineDeleteForm",
 *       "schema" = "Drupal\appsearch\Form\FieldSchemaForm",
 *       "reindex" = "Drupal\appsearch\Form\EngineReindexConfirmForm",
 *       "clear" = "Drupal\appsearch\Form\EngineClearIndexConfirmForm",
 *       "wipe_all" = "Drupal\appsearch\Form\EngineWipeAllConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\appsearch\EngineHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "appsearch_engine",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "engine" = "engine",
 *     "uuid" = "uuid",
 *     "language" = "language",
 *     "server" = "server",
 *     "datasources" = "datasources",
 *     "schema" = "schema",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "engine",
 *     "uuid",
 *     "language",
 *     "server",
 *     "datasources",
 *     "schema",
 *     "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/enterprise-search/appsearch/engine/{appsearch_engine}",
 *     "schema" = "/admin/config/search/enterprise-search/appsearch/engine/{appsearch_engine}/schema",
 *     "sync" = "/admin/config/search/enterprise-search/appsearch/engine/{appsearch_engine}/sync",
 *     "add-form" = "/admin/config/search/enterprise-search/appsearch/engine/add",
 *     "edit-form" = "/admin/config/search/enterprise-search/appsearch/engine/{appsearch_engine}/edit",
 *     "delete-form" = "/admin/config/search/enterprise-search/appsearch/engine/{appsearch_engine}/delete",
 *     "collection" = "/admin/config/search/enterprise-search/appsearch/engine"
 *   }
 * )
 */
class Engine extends ConfigEntityBase implements EngineInterface {

  /**
   * The Engine ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Engine label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Engine name that comes from AppSearch.
   *
   * @var string
   */
  protected $engine;

  /**
   * The Engine language.
   *
   * @var string
   */
  protected $language;

  /**
   * The Engine Server refrence.
   *
   * @var string
   */
  protected $server;

  /**
   * The Engine datasources refrence.
   *
   * @var array
   */
  protected $datasources;

  /**
   * The Engine datasources field schema.
   *
   * @var array
   */
  protected $schema;

  /**
   * The Engine status.
   *
   * @var bool
   */
  protected $status;

  /**
   * {@inheritdoc}
   */
  protected $trackerInstance;

  /**
   * {@inheritdoc}
   */
  public function getEngine() {
    return $this->engine;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguage() {
    return $this->language;
  }

  /**
   * {@inheritdoc}
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * {@inheritdoc}
   */
  public function datasources() {
    return $this->datasources;
  }

  /**
   * Returns the server instance.
   *
   * @return \Drupal\appsearch\Entity\Server
   *   The server instance.
   */
  public function getServerInstance() {
    return \Drupal::entityTypeManager()
      ->getStorage('appsearch_server')
      ->load($this->getServer());
  }

  /**
   * Returns the client instance.
   *
   * @return \Elastic\EnterpriseSearch\AppSearch\Client
   *   The client instance.
   */
  public function getClient() {
    if ($server = $this->getServerInstance()) {
      return $server->getClient();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDataSourcesFields() {
    $collection = [];
    if (!empty($this->datasources())) {
      foreach ($this->datasources() as $bundle) {
        $entityFieldManager = \Drupal::service('entity_field.manager');
        // @todo Add support for other entity types.
        $fields = $entityFieldManager->getFieldDefinitions('node', $bundle);
        foreach ($fields as $key => $field) {
          if (!isset($collection[$key])) {
            $collection[$key]['field'] = $field;
          }
          $collection[$key]['appears_in'][] = $bundle;
        }
      }
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function supportedtypes() {
    return [
      'text' => 'Text',
      'number' => 'Number',
      'date' => 'Date',
      'geolocation' => 'Geolocation',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setSchema($settings) {
    $this->schema = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getEngineFields($keysonly = FALSE) {
    $collection = [];
    if (!empty($this->schema)) {
      foreach ($this->schema as $schema) {
        if ($keysonly) {
          $collection[] = $schema['field_id'];
        }
        else {
          $collection[$schema['field_id']] = $schema;
        }

      }
    }
    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchema() {
    return $this->schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function info() {
    $elasticlient = $this->getClient();

    if (!$elasticlient) {
      return;
    }
    $engine = $elasticlient->getEngine(new GetEngine($this->getEngine()));

    if ($engine) {
      return $engine->asArray();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if ($this->isNew()) {
      try {
        $engine = $this->getClient()->getEngine(new GetEngine($this->getEngine()));
        $engine = $engine->asArray();
      }
      catch (\Exception $e) {
        \Drupal::logger('appsearch')->notice('Engine already exists on remote server - ' . $this->getEngine());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackerInstance() {
    $this->trackerInstance = \Drupal::service('appsearch.tracker')->getInstance($this);
    return $this->trackerInstance;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexItemsCount() {
    return Database::getNodeCount($this->datasources());
  }

  /**
   * {@inheritdoc}
   */
  public function setItemsTrackable() {
    $nodes = Database::getNodes($this->datasources());
    $this->getTrackerInstance()->trackAllItemsDeleted();
    $this->getTrackerInstance()->trackItemsInserted($nodes);
  }

  /**
   * {@inheritdoc}
   */
  public function indexDocuments(array $documents) {
    $indexable_docs = [];
    foreach ($documents as $document) {
      $doc = new Document($document['id']);
      unset($document['id']);
      foreach ($document as $key => $value) {
        $doc->$key = $value;
      }
      array_push($indexable_docs, $doc);
      unset($doc);
    }
    try {
      $result = $this->getClient()->indexDocuments(new IndexDocuments($this->getEngine(), $indexable_docs));
      $path = \Drupal::request()->getHost();
      \Drupal::logger('appsearch')->notice('Indexed ' . $this->getEngine() . ' from ' . $path);
      return $result;
    }
    catch (\Exception $e) {
      \Drupal::logger('appsearch')->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function listDocuments() {
    return $this->getClient()->listDocuments(new ListDocuments($this->getEngine()));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDocuments($documents) {
    return $this->getClient()->deleteDocuments(new DeleteDocuments($this->getEngine(), $documents));
  }

  /**
   * {@inheritdoc}
   */
  public function performTasks($tasks) {
    BatchHelper::setup($this, $tasks, ['limit' => 100, 'batch_size' => 100]);
  }

}
