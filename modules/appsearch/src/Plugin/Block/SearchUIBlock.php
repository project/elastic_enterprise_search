<?php

namespace Drupal\appsearch\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'SearchUiBlock' block.
 *
 * @Block(
 *  id = "searchui_block",
 *  admin_label = @Translation("AppSearch UI Block"),
 * )
 */
class SearchUiBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The search ui config entity.
   *
   * @var \Drupal\appsearch\Entity\SearchUi
   */
  protected $searchui;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SearchUiBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['searchui_settings'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Search UI Settings'),
      '#description' => $this->t('Provide the search ui settings to be used to render react package.'),
      '#options' => $this->getSearchUiSettings(),
      '#default_value' => (isset($this->configuration['searchui_settings'])) ? $this->configuration['searchui_settings'] : '',
      '#size' => 5,
      '#weight' => '0',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchUiSettings() {
    $collection = [];
    /** @var \Drupal\appsearch\Entity\SearchUi[] $uis */
    $uis = $this->entityTypeManager->getStorage('appsearch_searchui')->loadMultiple();
    foreach ($uis as $ui) {
      /** @var \Drupal\appsearch\Engine\EngineInterface $engine */
      $engine = $ui->getEngineInstance();
      if ($engine) {
        $server = $engine->getServerInstance();
        if ($server->status() && $server->isAvailable()) {
          $collection[$ui->id()] = sprintf(
            '%s -> %s -> %s',
            ucwords($server->label()),
            ucwords($ui->getEngine()),
            $ui->label()
          );
        }
      }
    }
    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['searchui_settings'] = $form_state->getValue('searchui_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $seachui_settings = $config['searchui_settings'];
    /** @var \Drupal\appsearch\Entity\SearchUi $searchui */
    $this->searchui = $this->entityTypeManager
      ->getStorage('appsearch_searchui')
      ->load($seachui_settings);

    // If searchui config entity is deleted that is being used in block config
    // then show message to admin to update block  with the block id and return
    // empty array.
    if (!$this->searchui) {
      $this->messenger()->addWarning(
        $this->t('The Search UI config entity with id %id is deleted. Please update the appsearch search ui block with new settings.', [
          '%id' => $seachui_settings,
        ])
      );
      return [];
    }

    $engine_data = $this->buildEngineJson($seachui_settings);

    $build = [];
    $build['#theme'] = 'searchui_block';
    $build['#content'][] = $seachui_settings;
    $build['#engine'] = $engine_data['json'];
    $build['#attached']['library'] = [
      'appsearch/appsearch-library',
    ];
    $inlinejs = '';
    $inlinejs = "var appConfig =  JSON.parse('" . json_encode($engine_data['json']) . "');";
    $inlinejs .= 'setTimeout(function(){
                      jQuery(".rc-pagination-item a").click(function(){
                          jQuery("html, body").animate({scrollTop:0},1e3,"swing")})},5e3);';
    $build['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => ['type' => 'text/javascript'],
        '#value' => $inlinejs,
      ],
      'appsearch_inlinejs',
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEngineJson($seachui_settings) {
    /** @var \Drupal\appsearch\Engine\EngineInterface $engine */
    $engine = $this->searchui->getEngineInstance();
    $server = $engine->getServerInstance();
    $engine_fields = $engine->getEngineFields();
    $engine_fields_key = $engine->getEngineFields(TRUE);
    $field_facets = $field_sort = [];
    $sorts = $this->searchui->getFieldsSort();
    $searchables = $this->searchui->getFieldsFilterSearchable();
    $field_sort[] = [
      'name' => $this->t('Relevance'),
      'value' => [],
    ];
    foreach ($sorts as $sort) {
      $field_sort[] = [
        'name' => $engine_fields[$sort]['label'],
        'value' => [
          [
            'field' => $sort,
            'direction' => 'asc',
          ],
        ],
      ];
    }

    $facets = $this->searchui->getFieldsFilterSearchable();
    foreach ($facets as $facet) {
      if ($facet) {
        $field_facets[$facet] = [
          'type' => 'value',
        ];
      }
    }

    $disjunctives = $this->searchui->getFieldsFilterDisjunctive();
    return [
      'json' => [
        'engineName' => $engine->id(),
        'endpointBase' => $server->getHost(),
        'searchKey' => $server->getSearchKey(),
        // Create an array from resultFields where key is the field name and
        // value is object with raw key.
        'resultFields' => array_combine($engine_fields_key, array_fill(0, count($engine_fields_key), ['raw' => (object) []])),
        'sortFields' => $field_sort,
        'facets' => $field_facets,
        'disjunctive' => array_keys($disjunctives),
        'titleField' => $this->searchui->getFieldTitle(),
        'urlField' => $this->searchui->getFieldUrl(),
        '_ctags' => $this->searchui->getCacheTags(),
        'searchables' => $searchables,
      ],
    ];
  }

}
