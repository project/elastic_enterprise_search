<?php

namespace Drupal\appsearch;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Search Ui entities.
 */
class SearchUiListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Search Ui');
    $header['id'] = $this->t('Machine name');
    $header['server'] = $this->t('Server');
    $header['engine'] = $this->t('Engine');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $servername = "";
    $engine = $entity->getEngineInstance();
    if ($engine) {
      $server = $engine->getServerInstance();
      if ($server) {
        $servername = $server->label();
      }
    }
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['server'] = $servername;
    $row['engine'] = $entity->getEngine();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $engine = \Drupal::request()->get('appsearch_engine');

    if (!empty($engine)) {
      $query = $this->getStorage()->getQuery()
        ->condition('engine', $engine, '=');
      return $query->execute();
    }

  }

}
