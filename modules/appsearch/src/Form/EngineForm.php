<?php

namespace Drupal\appsearch\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\appsearch\AppSearchClientInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EngineForm for engine config entity.
 */
class EngineForm extends EntityForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * App Search Client.
   *
   * @var \Drupal\appsearch\AppSearchClientInterface
   */
  protected $appSearchClient;

  /**
   * Constructs a ConfigEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\appsearch\AppSearchClientInterface $appSearchClient
   *   The App Search Client.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
  AppSearchClientInterface $appSearchClient) {
    $this->entityTypeManager = $entityTypeManager;
    $this->appSearchClient = $appSearchClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('appsearch.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, $appsearch_server = NULL, $appsearch_engine = NULL) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\appsearch\Entity\Engine $engine */
    $engine = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $engine->label(),
      '#description' => $this->t('Label for the Server.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $engine->id(),
      '#machine_name' => [
        'exists' => '\Drupal\appsearch\Entity\Engine::load',
      ],
      '#disabled' => !$engine->isNew(),
    ];

    $form['server'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Server'),
      '#maxlength' => 255,
      '#options' => $this->getActiveServers(),
      '#default_value' => !empty($engine->getServer()) ? $engine->getServer() : NULL,
      '#description' => $this->t("Please select the server."),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::updateEngineList',
        'wrapper' => 'engine-list',
        'event' => 'change',
      ],
    ];

    $engine_options = $this->getEngineNameListAssoc();
    $form['engine'] = [
      '#prefix' => '<div id="engine-list">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#title' => $this->t('Select Engine'),
      '#maxlength' => 255,
      '#options' => $engine_options,
      '#default_value' => !empty($engine_options) ? $engine->getEngine() : NULL,
      '#description' => $this->t("Please select the engine from selected server."),
      '#required' => TRUE,
    ];

    $form['datasources'] = [
      '#type' => 'select',
      '#title' => $this->t('Select content type'),
      '#multiple' => TRUE,
      '#size' => 20,
      '#options' => $this->getAvailbleContentTypes(),
      '#default_value' => $engine->datasources(),
      '#description' => $this->t("Select from the available content types to be index on engine"),
      '#required' => TRUE,
      '#attributes' => [
        'style' => 'width: 50em;',
      ],
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $engine->getStatus(),
      '#description' => $this->t('Enable disable the engine from here.'),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * The engine list update callback.
   */
  public function updateEngineList(array &$form, FormStateInterface $form_state) {
    return $form['engine'];
  }

  /**
   * Fetch available content types available in system.
   */
  public function getAvailbleContentTypes() {
    // @todo Add support for other entity types.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // If you need to display them in a drop down:
    $types = [];
    foreach ($node_types as $node_type) {
      $types[$node_type->id()] = $node_type->label();
    }
    return $types;
  }

  /**
   * Get available engines.
   */
  public function getActiveServers() {
    $servers = $this->entityTypeManager->getStorage('appsearch_server')->loadMultiple();

    $server_collection = ['' => $this->t('- Select -')];
    foreach ($servers as $key => $server) {
      if ($server->status() && $server->isAvailable()) {
        $server_collection[$key] = $server->label();
      }
    }

    return $server_collection;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Set entity label and id same as engine name.
    $engine = $this->entity;
    $status = $engine->save();

    if ($status) {
      $engine->setItemsTrackable();
    }

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Engine.', [
          '%label' => $engine->label(),
        ]));
        $redirect = $engine->toUrl('schema');
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Engine.', [
          '%label' => $engine->label(),
        ]));
        $redirect = $engine->toUrl('canonical');
    }
    $form_state->setRedirectUrl($redirect);
  }

  /**
   * Get the list of existing engines.
   */
  public function getExistingEngines() {
    // Load all engine of the selected server.
    $engines = $this->entityTypeManager->getStorage('appsearch_engine')->loadByProperties(['server' => $this->entity->getServer()]);
    $engine_collection = [];
    foreach ($engines as $engine) {
      $engine_collection[$engine->label()] = $engine->label();
    }
    return $engine_collection;
  }

  /**
   * Get the list of engines that are not yet created.
   */
  public function getNonExistingEngines() {
    $engine_options = [];
    $engine_names = $this->getEngineNameListAssoc();
    // Update the engine list field with the new list of engines that does not
    // exist in the system.
    $engine_options = array_diff($engine_names, $this->getExistingEngines());
    return $engine_options;
  }

  /**
   * Get Engine List.
   */
  public function getEngineNameListAssoc() {
    $server = $this->entity->getServer();
    $engine_names = [];
    if (!empty($server)) {
      // Get server entity instance.
      $server_entity = $this->entity->getServerInstance();
      // Get the list of engines for the selected server.
      $engines = $this->appSearchClient->getEngines($server_entity)->asArray();
      // Craete engine name array from result.
      $engine_names = array_column($engines['results'], 'name');
      $engine_names = array_combine($engine_names, $engine_names);
    }

    return $engine_names;
  }

}
