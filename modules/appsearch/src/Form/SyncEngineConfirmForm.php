<?php

namespace Drupal\appsearch\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\SearchApiException;
use Drupal\Core\Entity\EntityConfirmFormBase;

/**
 * Defines a confirm form for reindexing an index.
 */
class SyncEngineConfirmForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to sync %name, Make sure that the engine documents indexed on the server was
     originated from this drupal install only, You may otherwise sabotise the engine in case it is also in sync with
      other drupal instance?', ['%name' => 'engine name']);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Indexed data will be cleared on the search server.
                    Searches on this index will stop yielding results. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.appsearch_engine.canonical', ['appsearch_engine' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $appsearch_engine = NULL) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\search_api\IndexInterface $index */
    $engine = $this->getEntity();

    try {
      $engine->performTasks(['clear']);
    }
    catch (SearchApiException $e) {
      // Echo $e->getMessage(); exit;.
    }

    $form_state->setRedirect('entity.appsearch_engine.canonical', ['appsearch_engine' => $engine->id()]);
  }

}
