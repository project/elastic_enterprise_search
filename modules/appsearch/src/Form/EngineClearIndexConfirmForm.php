<?php

namespace Drupal\appsearch\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\search_api\SearchApiException;

/**
 * Defines a confirm form for reindexing an index.
 */
class EngineClearIndexConfirmForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to clear the search index %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Indexed data will be cleared on the search server.
                    Searches on this index will stop yielding results. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.appsearch_engine.canonical', ['appsearch_engine' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\search_api\IndexInterface $index */
    $engine = $this->getEntity();

    try {
      $engine->performTasks(['clear']);
    }
    catch (SearchApiException $e) {
      $this->logger('appsearch')->error($e->getMessage());
    }

    $form_state->setRedirect('entity.appsearch_engine.canonical', ['appsearch_engine' => $engine->id()]);
  }

}
