<?php

namespace Drupal\appsearch\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\UnsavedConfigurationInterface;
use Elastic\EnterpriseSearch\AppSearch\Request\PutSchema;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Elastic\EnterpriseSearch\AppSearch\Schema\SchemaUpdateRequest;

/**
 * Provides a form for configuring the fields of a search index.
 */
class FieldSchemaForm extends EntityForm {

  use MessengerTrait;

  /**
   * Constructs an IndexFieldsForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'appsearch_schema_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description']['#markup'] = $this->t('<p>The data type of a field determines how it can be used for searching and filtering.
     The boost is used to give additional weight to certain fields, for example titles or tags.</p>
     <p>For information about the data types available for indexing, see the <a href=":url">data types table</a>
      at the bottom of the page.</p>', [':url' => '#search-api-data-types-table']);
    $engine = $this->entity;
    $fields = $engine->getDataSourcesFields();
    $form_state->disableCache();

    // Set an appropriate page title.
    $form['#title'] = $this->t('Manage fields for search engine %label', ['%label' => $engine->label()]);
    $form['#tree'] = TRUE;

    if (!empty($fields)) {
      $form['_general'] = $this->buildFieldsTable($fields);
      $form['_general']['#title'] = $this->t('General');
    }

    $form['actions'] = $this->actionsElement($form, $form_state);

    return $form;
  }

  /**
   * Builds the form fields for a set of fields.
   *
   * @param \Drupal\search_api\Item\FieldInterface[] $fields
   *   List of fields to display.
   *
   * @return array
   *   The build structure.
   */
  protected function buildFieldsTable(array $fields) {

    $types = $this->entity->supportedtypes();
    $engine_fields = $this->entity->getEngineFields();

    $build = [
      '#type' => 'details',
      '#open' => TRUE,
      '#theme' => 'appsearch_admin_fields_table',
      '#parents' => [],
      '#header' => [
        $this->t('Label'),
        $this->t('Machine name'),
        $this->t('Appears In'),
        $this->t('Schema'),
        [
          'data' => $this->t('Enabled?'),
          'colspan' => 2,
        ],
      ],
    ];

    ksort($fields);

    foreach ($fields as $key => $field) {

      $title = (!empty($engine_fields) && isset($engine_fields[$key])) ?
        $engine_fields[$key]['label'] : $field['field']->getLabel();

      $build['fields'][$key]['title'] = [
        '#type' => 'textfield',
        '#default_value' => $title,
        '#required' => TRUE,
        // '#attributes' => ['readonly' => 'readonly'],
        '#size' => 40,
      ];
      $build['fields'][$key]['id'] = [
        '#type' => 'textfield',
        '#default_value' => $key,
        '#attributes' => ['readonly' => 'readonly'],
        '#required' => TRUE,
        '#size' => 35,
      ];

      $build['fields'][$key]['appears_in'] = [
        '#markup' => Html::escape(implode(', ', $field['appears_in'])),
      ];

      $build['fields'][$key]['type'] = [
        '#type' => 'select',
        '#options' => $types,
        '#default_value' => (isset($engine_fields[$key])) ? $engine_fields[$key]['type'] : 'text',
      ];

      $build['fields'][$key]['enable'] = [
        '#type' => 'checkbox',
        '#default_value' => (isset($engine_fields[$key])) ? 1 : 0,
      ];

    }

    return $build;
  }

  /**
   * Format schema to be easily saved as config entity.
   */
  public function formatSchema($fields, $json = FALSE) {
    $schema = [];
    foreach ($fields as $field) {
      if (isset($field['enable']) && $field['enable'] == 1) {
        if ($json) {
          $schema[$field['id']] = $field['type'];
        }
        else {
          $schema[] = [
            'label' => $field['title'],
            'field_id' => $field['id'],
            'type' => $field['type'],
          ];
        }
      }
    }
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $engine = $this->entity;

    $field_values = $form_state->getValue('fields', []);

    $engine->setSchema($this->formatSchema($field_values));

    $status = $engine->save();

    if ($status) {
      $engine->setItemsTrackable();
      if ($engine->getServerInstance()->isAvailable()) {
        $schema = new SchemaUpdateRequest();
        foreach ($field_values as $field => $field_settings) {
          // Create/Update schema for only enabled fields.
          if ($field_settings['enable']) {
            $schema->$field = $field_settings['type'];
          }
        }
        $engine->getClient()->putSchema(new PutSchema($engine->getEngine(), $schema));
      }
    }

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Engine.', [
          '%label' => $engine->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Engine.', [
          '%label' => $engine->label(),
        ]));
    }
    $form_state->setRedirectUrl($engine->toUrl('canonical'));
  }

  /**
   * Cancels the editing of the index's fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancel(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof UnsavedConfigurationInterface && $this->entity->hasChanges()) {
      $this->entity->discardChanges();
    }

    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));
  }

}
