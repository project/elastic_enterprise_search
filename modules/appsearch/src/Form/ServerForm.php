<?php

namespace Drupal\appsearch\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ServerForm for server config entity.
 */
class ServerForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $server = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $server->label(),
      '#description' => $this->t('Label for the Server.'),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Short description of the server.'),
      '#maxlength' => 255,
      '#default_value' => $server->getDescription(),
      '#description' => $this->t('Label for the Server.'),
      '#required' => FALSE,
    ];

    $form['host'] = [
      '#type' => 'url',
      '#title' => $this->t('Backend Endpoint'),
      '#maxlength' => 255,
      '#default_value' => $server->getHost(),
      '#description' => $this->t('You can find the API endpoint URL in the credentials sections of the App Search'),
      '#required' => TRUE,
    ];

    $form['privateKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#maxlength' => 255,
      '#default_value' => $server->getPrivateKey(),
      '#description' => $this->t('Private key provided in App Search Credentials. Starts with private-'),
      '#required' => TRUE,
    ];

    $form['searchKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Key'),
      '#maxlength' => 255,
      '#default_value' => $server->getSearchKey(),
      '#description' => $this->t('Search key provided in App Search Credentials. Starts with search-'),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $server->getStatus(),
      '#description' => $this->t('Enable disable the server from here.'),
      '#required' => FALSE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $server->id(),
      '#machine_name' => [
        'exists' => '\Drupal\appsearch\Entity\Server::load',
      ],
      '#disabled' => !$server->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $server = $this->entity;
    $status = $server->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Server.', [
          '%label' => $server->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Server.', [
          '%label' => $server->label(),
        ]));
    }
    $form_state->setRedirectUrl($server->toUrl('collection'));
  }

}
