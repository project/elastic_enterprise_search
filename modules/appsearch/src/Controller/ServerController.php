<?php

namespace Drupal\appsearch\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\appsearch\Entity\ServerInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Class EngineController.
 */
class ServerController extends ControllerBase {

  /**
   * Returns the page title for an engine's "View" tab.
   *
   * @param \Drupal\appsearch\ServerInterface $appsearch_server
   *   The index that is displayed.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(ServerInterface $appsearch_server) {
    return new FormattableMarkup('@title', ['@title' => $appsearch_server->label()]);
  }

  /**
   * Displays information about a engine.
   *
   * @param \Drupal\appsearch\ServerInterface $appsearch_server
   *   The index to display.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(ServerInterface $appsearch_server) {
    // Build the search index information.
    $render = [
      'view' => [
        '#theme' => 'server',
        '#server' => $appsearch_server,
      ],
    ];

    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function schema($appsearch_engine) {

    return [
      'view' => [
        '#theme' => 'table',
        '#engine' => $appsearch_engine,
      ],
    ];
  }

}
