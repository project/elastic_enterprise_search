<?php

namespace Drupal\appsearch\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\appsearch\Entity\EngineInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Class EngineController to handle the engine related operations.
 */
class EngineController extends ControllerBase {

  /**
   * Returns the page title for an engine's "View" tab.
   *
   * @param \Drupal\elastic_enterprise_search\EngineInterface $appsearch_engine
   *   The index that is displayed.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(EngineInterface $appsearch_engine) {
    return new FormattableMarkup('@title', ['@title' => $appsearch_engine->label()]);
  }

  /**
   * Displays information about a engine.
   *
   * @param \Drupal\appsearch\EngineInterface $appsearch_engine
   *   The index to display.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(EngineInterface $appsearch_engine) {
    // Build the search index information.
    $render = [
      'view' => [
        '#theme' => 'engine',
        '#engine' => $appsearch_engine,
      ],
    ];

    if (
      $appsearch_engine->status() && $appsearch_engine->getServerInstance()->isAvailable()) {
      // Attach the index status form.
      $render['form'] = $this->formBuilder()->getForm('Drupal\appsearch\Form\EngineStatusForm', $appsearch_engine);
    }
    return $render;
  }

}
