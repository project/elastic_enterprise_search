<?php

namespace Drupal\appsearch\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the appsearch module.
 */
class EngineControllerTest extends WebTestBase {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "appsearch EngineController's controller functionality",
      'description' => 'Test Unit for module appsearch and controller EngineController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests appsearch functionality.
   */
  public function testEngineController() {
    // Check that the basic functions of module appsearch.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
