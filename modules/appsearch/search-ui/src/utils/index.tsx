// Helper function that will check if component is avaialbe under
// ./components-overrides folder and return it if it is, otherwise return the
// fallback to the original component.
export const getComponent = (componentName: string) => {
  try {
    return require(`../searchui_components_override/${componentName}`).default;
  } catch (e) {
    // eslint-disable-next-line no-console
    console.warn(
      `Component ${componentName} is not available under searchui_components_override folder. Using default component.`
    );
    return require(`../components/${componentName}`).default;
  }
}

export const truncate = (str: string, n: number, useWordBoundary: boolean) => {

  if (str === 'undefined' || str === null) { return ""; }

  str = str.trim();

  if (str.length <= n) { return str; }
  const subString = str.substr(0, n - 1); // the original check
  return (useWordBoundary
    ? subString.substr(0, subString.lastIndexOf(" "))
    : subString) + "&hellip;";
}

