// window.appConfig is only avaialbe in browser, not in node when building the
// app. This file is used to set the appConfig variable so that it can be used
// in node when building the app. In browser it will pick up the value from
// window.appConfig.
window.appConfig = window.appConfig || {};
export const appConfig = {
  searchKey : window?.appConfig?.searchKey,
  engineName: window?.appConfig?.engineName,
  endpointBase: window?.appConfig?.endpointBase,
  resultFields: window?.appConfig?.resultFields,
  sortFields: window?.appConfig?.sortFields,
  facets: window?.appConfig?.facets,
  disjunctive: window?.appConfig?.disjunctive,
  titleField: window?.appConfig?.titleField,
  urlField: window?.appConfig?.urlField,
  _ctags: window?.appConfig?._ctags,
  searchables: window?.appConfig?.searchables,
}
