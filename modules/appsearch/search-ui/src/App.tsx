import React from "react";
import AppSearchAPIConnector from "@elastic/search-ui-app-search-connector";
import {appConfig} from './env_setup';
import {getComponent} from "./utils";
import {
  ErrorBoundary,
  Facet,
  SearchProvider,
  SearchBox,
  Results,
  PagingInfo,
  ResultsPerPage,
  Paging,
  Sorting,
  WithSearch
} from "@elastic/react-search-ui";
import {
  BooleanFacet,
  Layout,
  SingleLinksFacet,
  SingleSelectFacet
} from "@elastic/react-search-ui-views";
import "@elastic/react-search-ui-views/lib/styles/styles.css";
import { SearchDriverOptions } from "@elastic/search-ui";

const ResultView = getComponent("ResultView");

const connector = new AppSearchAPIConnector({
  searchKey: appConfig?.searchKey,
  engineName: appConfig?.engineName,
  endpointBase: appConfig?.endpointBase,
});

const config: SearchDriverOptions = {
  alwaysSearchOnInitialLoad: true,
  apiConnector: connector,
  hasA11yNotifications: true,
  searchQuery: {
    result_fields: appConfig?.resultFields,
    disjunctiveFacets: appConfig?.disjunctive,
    facets: appConfig?.facets,
  },
  autocompleteQuery: {
    results: {
      resultsPerPage: 5,
      result_fields: {
        title: {
          snippet: {
            size: 100,
            fallback: true,
          }
        },
        path: {
          raw: {}
        }
      }
    },
    suggestions: {
      types: {
        documents: {
          fields: ["title"]
        }
      },
      size: 5
    }
  }
};
const SORT_OPTIONS = appConfig.sortFields;

export default function App() {
  return (
    <SearchProvider config={config}>
      <WithSearch
        mapContextToProps={({ wasSearched }) => ({
          wasSearched
        })}
      >
        {({ wasSearched }) => {
          return (
            <div className="App">
              <ErrorBoundary>
                <Layout
                  header={
                    <SearchBox
                      autocompleteMinimumCharacters={3}
                      autocompleteResults={{
                        linkTarget: "_blank",
                        sectionTitle: "Results",
                        titleField: "title",
                        urlField: "path",
                        shouldTrackClickThrough: true,
                        clickThroughTags: ["search-box-autocomplete-click"]
                      }}
                      autocompleteSuggestions={true}
                      debounceLength={0}
                    />
                  }
                  sideContent={
                    <div>
                      {wasSearched && (
                        <Sorting label={"Sort by"} sortOptions={SORT_OPTIONS} />
                      )}
                      {/* Loop through appConfig.facets object and create Facet */}
                      {wasSearched && (
                        <React.Fragment>
                          {Object.keys(appConfig.facets).map((facet) => {
                            return (
                              <Facet
                                key={facet}
                                field={facet}
                                label={facet}
                                filterType="any"
                                isFilterable={true}
                              />
                            );
                          })}
                        </React.Fragment>
                      )}
                    </div>
                  }
                  bodyContent={
                    <Results
                      titleField={appConfig.titleField}
                      urlField={appConfig.urlField}
                      thumbnailField="field_image"
                      resultView={ResultView}
                      shouldTrackClickThrough={true}
                    />
                  }
                  bodyHeader={
                    <React.Fragment>
                      {wasSearched && <PagingInfo />}
                      {wasSearched && <ResultsPerPage />}
                    </React.Fragment>
                  }
                  bodyFooter={<Paging />}
                />
              </ErrorBoundary>
            </div>
          );
        }}
      </WithSearch>
    </SearchProvider>
  );
}
