import React from "react";

export default (props) => {
  let result = props.result;
  const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  let date = null;
  let formatted = '';

  date = result.created && result.created.raw ? new Date(parseInt(result.created.raw) * 1000) : null;
  if (date) {
    formatted = month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
  }

  return (
    <div>
      <span className="card__date sui-result__date">{formatted}</span>
    </div>
  )
}
