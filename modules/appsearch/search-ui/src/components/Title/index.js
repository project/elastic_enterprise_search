import React from "react";

export default (props) => {
  const result = props.result;
  return (
    <span className="sui-result__title"
      dangerouslySetInnerHTML={{ __html: result.title.raw }}
    />
  )
}
