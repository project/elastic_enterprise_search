import React from "react";
import { WithSearch } from "@elastic/react-search-ui";
import {getComponent} from "../../utils";

const Date = getComponent("Date");
const Description = getComponent("Description");
const Image = getComponent("Image");
const Title = getComponent("Title");

// import { ClickStore } from "./ClickStore";

const ResultItem = ({ result, onClickLink }) => {
  return (
    <li className={`sui-result searchDiv sui-${result.type.raw} ${(result.hasOwnProperty('field_is_event_online') && result.field_is_event_online.raw) ? 'sui-event-online' : 'off'}`

    }>
      <div className="card-list card-list--teaser card-list--teaser-search adipoli">
        <div className="sui-result__header">
          <a onClick={onClickLink} href={result.title.raw}>
            <Title result={result} />
          </a>
        </div>
        <div className="sui-result__body">
          <ul className="sui-result__details">
            <li>
              <div className="sui-result__value" >
                <Image result={result} />
                <Description result={result} />
              </div>
            </li>
            <li>
              <Date result={result} />
            </li>
          </ul>
        </div>
      </div>
    </li>
  )
};

export default (props) => {

  return (<WithSearch mapContextToProps={({ searchTerm }) => ({ searchTerm })}>
    {({ searchTerm }) => {
      return (
        <ResultItem {...props} searchTerm={searchTerm} />
      )
    }}
  </WithSearch >
  );
}
