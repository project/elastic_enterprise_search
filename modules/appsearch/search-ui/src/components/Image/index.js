import React from "react";

export default (props) => {
  let image = '';
  const result = props.result;
  image = result.field_image && result.field_image.raw ? result.field_image.raw : '';

  if (image) {
    return (
      <span className="blog-img float-lg-right ml-lg-3"><img height="220px" width="220px" alt={image} src={image} /></span>
    )
  }

  return (null)
}
