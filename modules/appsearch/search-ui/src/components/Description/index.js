import React from "react";
import { truncate } from "../../utils";

export default (props) => {
  const result = props.result;
  let body = result.body && result.body.snippet ? result.body.snippet : result.body && result.body.raw ? result.body.raw : '';
  body = truncate(body, 600, true);

  return (
    <div className="sui-result__value"
      dangerouslySetInnerHTML={{ __html: body }}
    />
  )
}

